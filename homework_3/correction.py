#!/usr/bin/env python3

# 1/86 tuple + range

my_tuple = tuple(range(1, 101))
print(my_tuple)
print()

# 2/86 (u, w, x, y, z)

u, w, x, y, z = (1, 2, 3, 4, 5)
print(u, w, x, y, z)
print()

# 1/88 key:value with for(each)

user_dict = {}

for num in range(5):
    key = input("Write key number " + str(num + 1) + ": ")
    user_dict[key] = input("Write value of key number " + str(num + 1) + ": ")

print()
print(user_dict)
