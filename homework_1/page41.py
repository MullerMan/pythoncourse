#!/usr/bin/env python3


class Tree: pass


def my_name() -> None:
    print("Przemek")


def one_plus_three_plus_five() -> None:
    print(1 + 3 + 5)


def hello_world() -> str:
    return "Cześć świecie"


def subtraction(first_num: int, second_num: int) -> int:
    return first_num - second_num


def creating_tree_object() -> Tree:
    oak = Tree()
    return oak


# verification of functions
my_name()
one_plus_three_plus_five()
print(hello_world())
print(subtraction(10, 4))
print(isinstance(creating_tree_object(), Tree))
