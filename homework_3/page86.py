#!/usr/bin/env python3

# first bulletpoint -> tuple + range

a = []

for num in range(0, 101):
    a.append(num)

b = tuple(a)
print(b)

# second bulletpoint -> 5 element tuple

five_elements_tuple = (1, 2, 3, 4, 5)
u = five_elements_tuple[0]
v = five_elements_tuple[1]
x = five_elements_tuple[2]
y = five_elements_tuple[3]
z = five_elements_tuple[4]

print()
print(u, v, x, y, z)
