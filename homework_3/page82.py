#!/usr/bin/env python3

# first bulletpoint -> 3 lists with 1 element

main_list = [[1], [2], [3]]

# second bulletpoint -> list with 100-115

container = []

for num in range(100, 116):
    container.append(num)

print(container)
print()

# third bulletpoint -> list with floats

floats = [1.1, 3.3, 5.5, 7.7, 11.11]
for element in floats:
    print(element)
