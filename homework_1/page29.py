#!/usr/bin/env python3

# first bulletpoint "fauna class"
class Fish:
    def swim(self) -> None:
        print("Fish swims in water.")

    def eat(self) -> None:
        print("Fish eats plants and smaller fishes.")

    def sleep(self) -> None:
        print("Fish sleeps in water.")


salmon = Fish()
salmon.swim()
salmon.eat()
salmon.sleep()
print()


# second bulletpoint "cosmetic class"
class FaceCream:
    def description(self) -> None:
        print("Face cream protects your face skin.")

    def availability(self) -> None:
        print("Face cream is available in every pharmacy.")

    def tips(self) -> None:
        print("Face cream should be applicated on face everyday.")


garnier = FaceCream()
garnier.description()
garnier.availability()
garnier.tips()
print()


# third bulletpoint "bakery class"
class Croissant:
    def short_info(self) -> None:
        print("Croissant is a sweet snack.")

    def taste(self) -> None:
        print("Croissant has different tastes. It could be chocolate or vanilla.")

    def price(self) -> None:
        print("Croissant is cheap.")


seven_days = Croissant()
seven_days.short_info()
seven_days.taste()
seven_days.price()
