#!/usr/bin/env python3

# first bulletpoint -> name, age input and output


first_name: str = input("Give first name: ")
surname: str = input("Give surname: ")
age: str = input("Give your age: ")

print("You are", first_name, surname, "and you are", age, "years old.")
print()


# second bulletpoint -> function to convert input from float to int


def float_to_int() -> None:
    floating_point_num: float = float(input("Please, give floating point number: "))
    print("Here your integer: ", int(floating_point_num))


float_to_int()
print()


# third bulletpoint -> class that describe flower


class Flower:
    def __init__(self, name, color):
        self.name = name
        self.color = color

    def description(self):
        print("Your flower is called", self.name, "and it's color is", self.color)


flower_name: str = input("What's your flower name?")
flower_color: str = input("What's your flower color?")

flower = Flower(flower_name, flower_color)
flower.description()
print()
