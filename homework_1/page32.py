#!/usr/bin/env python3

class Nature:
    pass


class Animals(Nature):
    pass


class Birds(Animals):
    pass


class Stork(Birds):
    pass


choice = Animals()

print(isinstance(choice, Nature))
print(isinstance(choice, Animals))
print(isinstance(choice, Birds))
print(isinstance(choice, Stork))
