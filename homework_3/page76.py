#!/usr/bin/env python3

# first bulletpoint -> while + randrange

import random

counter = 0
while counter != 20:
    counter = random.randrange(15, 25)
    print(counter, end=" ")
print("\n")

# second bulletpoint -> for + range

for num in range(20, -1, -1):
    print(num)
