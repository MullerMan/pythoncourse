#!/usr/bin/env python3

# first bulletpoint -> 5 element dict

user_dict = {}

for num in range(0, 5):
    key = input("Write key number " + str(num + 1) + ": ")
    user_dict[key] = input("Write value of key number " + str(num + 1) + ": ")

print()
print(user_dict)

# second bulletpoint -> dict with 5 lists[5 elements]

flowers = ["carnation", "cactus", "tulip", "violet", "rose"]
animals = ["hippo", "giraffe", "ostrich", "goat", "mosquito"]
furniture = ["wardrobe", "bookshelf", "bed", "desk", "table"]
names = ["jack", "george", "michael", "elton", "john"]
dishes = ["hamburger", "chicken soup", "fish and chips", "spaghetti", "pizza"]

database = {"flowers": flowers,
            "animals": animals,
            "furniture": furniture,
            "names": names,
            "dishes": dishes}
print(database)
