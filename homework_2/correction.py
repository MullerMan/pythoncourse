#!/usr/bin/env python3

# page 50 / first bulletpoint -> variable inheritance
class Human:
    def __init__(self, arms: int, fingers: int):
        self.arms = arms
        self.fingers = fingers


class Grandma(Human):
    pass


elice = Grandma(2, 20)
print("This human has", elice.arms, "arms and", elice.fingers, "fingers")


# page 71 / fourth bulletpoint -> 6 classes

class Insect:
    def __init__(self, name: str, legs: int):
        self.name = name
        self.legs = legs

    def __eq__(self, other):
        print("Comparison type(legs): {} == {}".format(self.name, other.name))
        return self.legs == other.legs

    def __lt__(self, other):
        print("Comparison type(legs): {} < {}".format(self.name, other.name))
        return self.legs < other.legs

    def __gt__(self, other):
        print("Comparison type(legs): {} > {}".format(self.name, other.name))
        return self.legs > other.legs

    def __le__(self, other):
        print("Comparison type(legs): {} <= {}".format(self.name, other.name))
        return self.legs <= other.legs

    def __ge__(self, other):
        print("Comparison type(legs): {} >= {}".format(self.name, other.name))
        return self.legs >= other.legs

    def __ne__(self, other):
        print("Comparison type(legs): {} != {}".format(self.name, other.name))
        return self.legs != other.legs


class Grasshopper(Insect):
    def __init__(self, name: str, legs: int):
        super().__init__(name, legs)

    def __eq__(self, other):
        return super().__eq__(other)

    def __lt__(self, other):
        return super().__lt__(other)

    def __gt__(self, other):
        return super().__gt__(other)

    def __le__(self, other):
        return super().__le__(other)

    def __ge__(self, other):
        return super().__ge__(other)

    def __ne__(self, other):
        return super().__ne__(other)


class Ant(Insect):
    def __init__(self, name: str, legs: int):
        super().__init__(name, legs)

    def __eq__(self, other):
        return super().__eq__(other)

    def __lt__(self, other):
        return super().__lt__(other)

    def __gt__(self, other):
        return super().__gt__(other)

    def __le__(self, other):
        return super().__le__(other)

    def __ge__(self, other):
        return super().__ge__(other)

    def __ne__(self, other):
        return super().__ne__(other)


class FlyingInsect(Insect):
    def __init__(self, name: str, legs: int):
        super().__init__(name, legs)

    def __eq__(self, other):
        return super().__eq__(other)

    def __lt__(self, other):
        return super().__lt__(other)

    def __gt__(self, other):
        return super().__gt__(other)

    def __le__(self, other):
        return super().__le__(other)

    def __ge__(self, other):
        return super().__ge__(other)

    def __ne__(self, other):
        return super().__ne__(other)


class LadyBug(FlyingInsect):
    def __init__(self, name: str, legs: int):
        super().__init__(name, legs)

    def __eq__(self, other):
        return super().__eq__(other)

    def __lt__(self, other):
        return super().__lt__(other)

    def __gt__(self, other):
        return super().__gt__(other)

    def __le__(self, other):
        return super().__le__(other)

    def __ge__(self, other):
        return super().__ge__(other)

    def __ne__(self, other):
        return super().__ne__(other)


class Beetle(FlyingInsect):
    def __init__(self, name: str, legs: int):
        super().__init__(name, legs)

    def __eq__(self, other):
        return super().__eq__(other)

    def __lt__(self, other):
        return super().__lt__(other)

    def __gt__(self, other):
        return super().__gt__(other)

    def __le__(self, other):
        return super().__le__(other)

    def __ge__(self, other):
        return super().__ge__(other)

    def __ne__(self, other):
        return super().__ne__(other)


mosquito = FlyingInsect("Mosquito", 6)
beetle = Beetle("Beetle", 6)

print()
print("1. Mosquito legs == Beetle legs = {}".format(mosquito == beetle))
print("2. Mosquito legs < Beetle legs = {}".format(mosquito < beetle))
print("3. Mosquito legs > Beetle legs = {}".format(mosquito > beetle))
print("4. Mosquito legs <= Beetle legs = {}".format(mosquito <= beetle))
print("5. Mosquito legs >= Beetle legs = {}".format(mosquito >= beetle))
print("6. Mosquito legs != Beetle legs = {}".format(mosquito != beetle))
print()

# page 72 / third bulletpoint -> cuboid volume class


class Cuboid:
    def __init__(self, name: str, x: int, y: int,  z: int,):
        self.name = name
        self.x = x
        self.y = y
        self.z = z

    @property
    def volume(self) -> int:
        return self.x * self.y * self.z

    def __eq__(self, other):
        print("Comparison type: {} == {}".format(self.name, other.name))
        return self.volume == other.volume

    def __lt__(self, other):
        print("Comparison type: {} < {}".format(self.name, other.name))
        return self.volume < other.volume

    def __gt__(self, other):
        print("Comparison type: {} > {}".format(self.name, other.name))
        return self.volume > other.volume

    def __le__(self, other):
        print("Comparison type: {} <= {}".format(self.name, other.name))
        return self.volume <= other.volume

    def __ge__(self, other):
        print("Comparison type: {} >= {}".format(self.name, other.name))
        return self.volume >= other.volume

    def __ne__(self, other):
        print("Comparison type: {} != {}".format(self.name, other.name))
        return self.volume != other.volume


cuboid_1 = Cuboid("Cuboid1", 2, 3, 4)
cuboid_2 = Cuboid("Cuboid2", 3, 4, 5)

print("1. Cuboid1 == Cuboid2 = {}".format(cuboid_1 == cuboid_2))
print("2. Cuboid1 < Cuboid2 = {}".format(cuboid_1 < cuboid_2))
print("3. Cuboid1 > Cuboid2 = {}".format(cuboid_1 > cuboid_2))
print("4. Cuboid1 <= Cuboid2 = {}".format(cuboid_1 <= cuboid_2))
print("5. Cuboid1 >= Cuboid2 = {}".format(cuboid_1 >= cuboid_2))
print("6. Cuboid1 != Cuboid2 = {}".format(cuboid_1 != cuboid_2))
print()
