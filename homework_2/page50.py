#!/usr/bin/env python3

# first bulletpoint -> variable inheritance
class Human:
    arms = 2
    fingers = 20


class Grandma(Human):
    pass


elice = Grandma()
print("This human has", elice.arms, "arms and", elice.fingers, "fingers")


# second bulletpoint -> method inheritance
class Tree:
    def print_age(self, age: int) -> None:
        print("This tree is", age, "years old.")


class Oak(Tree):
    pass


bartek = Oak()
bartek.print_age(100)


# third bulletpoint -> method inheritance + super()
class Bird:
    def fly(self, name: str):
        print(name + " is a bird")


class Penguin(Bird):
    def fly(self, name: str):
        print(name + " is a penguin")
        super().fly("Judy")


judy = Penguin()
judy.fly("Judy")
