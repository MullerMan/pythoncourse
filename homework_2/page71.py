#!/usr/bin/env python3

# first bulletpoint -> name_equal_name function

def name_equal_name():
    name: str = input("Please, give me your name: ")
    if name == "Przemek":
        print("Hello Przemek!")
    else:
        print("Hello World!")


name_equal_name()
print()


# second bulletpoint -> Circle class

class Circle:
    def __init__(self, radius_length: float, name: str):
        self.radius_length: float = radius_length
        self.name: str = name

    def __eq__(self, other):
        print("Comparison type: {} == {}".format(self.name, other.name))
        return self.radius_length == other.radius_length

    def __lt__(self, other):
        print("Comparison type: {} < {}".format(self.name, other.name))
        return self.radius_length < other.radius_length

    def __gt__(self, other):
        print("Comparison type: {} > {}".format(self.name, other.name))
        return self.radius_length > other.radius_length

    def __le__(self, other):
        print("Comparison type: {} <= {}".format(self.name, other.name))
        return self.radius_length <= other.radius_length

    def __ge__(self, other):
        print("Comparison type: {} >= {}".format(self.name, other.name))
        return self.radius_length >= other.radius_length

    def __ne__(self, other):
        print("Comparison type: {} != {}".format(self.name, other.name))
        return self.radius_length != other.radius_length


a = Circle(4.5, "A")
b = Circle(5.5, "B")

print("1. A == B = {}".format(a == b))
print("2. A < B = {}".format(a < b))
print("3. A > B = {}".format(a > b))
print("4. A <= B = {}".format(a <= b))
print("5. A >= B = {}".format(a >= b))
print("6. A != B = {}".format(a != b))
print()


# third bulletpoint -> Cuboid volume class


class Cuboid:
    def __init__(self, name: str, x: int, y: int,  z: int,):
        self.name = name
        self.x = x
        self.y = y
        self.z = z

    def __eq__(self, other):
        print("Comparison type: {} == {}".format(self.name, other.name))
        return self.x * self.y * self.z == other.x * other.y * other.z

    def __lt__(self, other):
        print("Comparison type: {} < {}".format(self.name, other.name))
        return self.x * self.y * self.z < other.x * other.y * other.z

    def __gt__(self, other):
        print("Comparison type: {} > {}".format(self.name, other.name))
        return self.x * self.y * self.z > other.x * other.y * other.z

    def __le__(self, other):
        print("Comparison type: {} <= {}".format(self.name, other.name))
        return self.x * self.y * self.z <= other.x * other.y * other.z

    def __ge__(self, other):
        print("Comparison type: {} >= {}".format(self.name, other.name))
        return self.x * self.y * self.z >= other.x * other.y * other.z

    def __ne__(self, other):
        print("Comparison type: {} != {}".format(self.name, other.name))
        return self.x * self.y * self.z != other.x * other.y * other.z


cuboid_1 = Cuboid("Cuboid1", 2, 3, 4)
cuboid_2 = Cuboid("Cuboid2", 3, 4, 5)

print("1. Cuboid1 == Cuboid2 = {}".format(cuboid_1 == cuboid_2))
print("2. Cuboid1 < Cuboid2 = {}".format(cuboid_1 < cuboid_2))
print("3. Cuboid1 > Cuboid2 = {}".format(cuboid_1 > cuboid_2))
print("4. Cuboid1 <= Cuboid2 = {}".format(cuboid_1 <= cuboid_2))
print("5. Cuboid1 >= Cuboid2 = {}".format(cuboid_1 >= cuboid_2))
print("6. Cuboid1 != Cuboid2 = {}".format(cuboid_1 != cuboid_2))
print()


# fourth bulletpoint -> 6 classes

class Insect:
    def __init__(self, name: str, legs: int):
        self.name = name
        self.legs = legs

    def __eq__(self, other):
        print("Comparison type(legs): {} == {}".format(self.name, other.name))
        return self.legs == other.legs

    def __lt__(self, other):
        print("Comparison type(legs): {} < {}".format(self.name, other.name))
        return self.legs < other.legs

    def __gt__(self, other):
        print("Comparison type(legs): {} > {}".format(self.name, other.name))
        return self.legs > other.legs

    def __le__(self, other):
        print("Comparison type(legs): {} <= {}".format(self.name, other.name))
        return self.legs <= other.legs

    def __ge__(self, other):
        print("Comparison type(legs): {} >= {}".format(self.name, other.name))
        return self.legs >= other.legs

    def __ne__(self, other):
        print("Comparison type(legs): {} != {}".format(self.name, other.name))
        return self.legs != other.legs


class Grasshopper(Insect):
    def __init__(self, name: str, legs: int):
        self.name = name
        self.legs = legs

    def __eq__(self, other):
        print("Comparison type(legs): {} == {}".format(self.name, other.name))
        return self.legs == other.legs

    def __lt__(self, other):
        print("Comparison type(legs): {} < {}".format(self.name, other.name))
        return self.legs < other.legs

    def __gt__(self, other):
        print("Comparison type(legs): {} > {}".format(self.name, other.name))
        return self.legs > other.legs

    def __le__(self, other):
        print("Comparison type(legs): {} <= {}".format(self.name, other.name))
        return self.legs <= other.legs

    def __ge__(self, other):
        print("Comparison type(legs): {} >= {}".format(self.name, other.name))
        return self.legs >= other.legs

    def __ne__(self, other):
        print("Comparison type(legs): {} != {}".format(self.name, other.name))
        return self.legs != other.legs


class Ant(Insect):
    def __init__(self, name: str, legs: int):
        self.name = name
        self.legs = legs

    def __eq__(self, other):
        print("Comparison type(legs): {} == {}".format(self.name, other.name))
        return self.legs == other.legs

    def __lt__(self, other):
        print("Comparison type(legs): {} < {}".format(self.name, other.name))
        return self.legs < other.legs

    def __gt__(self, other):
        print("Comparison type(legs): {} > {}".format(self.name, other.name))
        return self.legs > other.legs

    def __le__(self, other):
        print("Comparison type(legs): {} <= {}".format(self.name, other.name))
        return self.legs <= other.legs

    def __ge__(self, other):
        print("Comparison type(legs): {} >= {}".format(self.name, other.name))
        return self.legs >= other.legs

    def __ne__(self, other):
        print("Comparison type(legs): {} != {}".format(self.name, other.name))
        return self.legs != other.legs


class FlyingInsect(Insect):
    def __init__(self, name: str, legs: int):
        self.name = name
        self.legs = legs

    def __eq__(self, other):
        print("Comparison type(legs): {} == {}".format(self.name, other.name))
        return self.legs == other.legs

    def __lt__(self, other):
        print("Comparison type(legs): {} < {}".format(self.name, other.name))
        return self.legs < other.legs

    def __gt__(self, other):
        print("Comparison type(legs): {} > {}".format(self.name, other.name))
        return self.legs > other.legs

    def __le__(self, other):
        print("Comparison type(legs): {} <= {}".format(self.name, other.name))
        return self.legs <= other.legs

    def __ge__(self, other):
        print("Comparison type(legs): {} >= {}".format(self.name, other.name))
        return self.legs >= other.legs

    def __ne__(self, other):
        print("Comparison type(legs): {} != {}".format(self.name, other.name))
        return self.legs != other.legs


class LadyBug(FlyingInsect):
    def __init__(self, name: str, legs: int):
        self.name = name
        self.legs = legs

    def __eq__(self, other):
        print("Comparison type(legs): {} == {}".format(self.name, other.name))
        return self.legs == other.legs

    def __lt__(self, other):
        print("Comparison type(legs): {} < {}".format(self.name, other.name))
        return self.legs < other.legs

    def __gt__(self, other):
        print("Comparison type(legs): {} > {}".format(self.name, other.name))
        return self.legs > other.legs

    def __le__(self, other):
        print("Comparison type(legs): {} <= {}".format(self.name, other.name))
        return self.legs <= other.legs

    def __ge__(self, other):
        print("Comparison type(legs): {} >= {}".format(self.name, other.name))
        return self.legs >= other.legs

    def __ne__(self, other):
        print("Comparison type(legs): {} != {}".format(self.name, other.name))
        return self.legs != other.legs


class Beetle(FlyingInsect):
    def __init__(self, name: str, legs: int):
        self.name = name
        self.legs = legs

    def __eq__(self, other):
        print("Comparison type(legs): {} == {}".format(self.name, other.name))
        return self.legs == other.legs

    def __lt__(self, other):
        print("Comparison type(legs): {} < {}".format(self.name, other.name))
        return self.legs < other.legs

    def __gt__(self, other):
        print("Comparison type(legs): {} > {}".format(self.name, other.name))
        return self.legs > other.legs

    def __le__(self, other):
        print("Comparison type(legs): {} <= {}".format(self.name, other.name))
        return self.legs <= other.legs

    def __ge__(self, other):
        print("Comparison type(legs): {} >= {}".format(self.name, other.name))
        return self.legs >= other.legs

    def __ne__(self, other):
        print("Comparison type(legs): {} != {}".format(self.name, other.name))
        return self.legs != other.legs


mosquito = FlyingInsect("Mosquito", 6)
beetle = Beetle("Beetle", 6)

print("1. Mosquito legs == Beetle legs = {}".format(mosquito == beetle))
print("2. Mosquito legs < Beetle legs = {}".format(mosquito < beetle))
print("3. Mosquito legs > Beetle legs = {}".format(mosquito > beetle))
print("4. Mosquito legs <= Beetle legs = {}".format(mosquito <= beetle))
print("5. Mosquito legs >= Beetle legs = {}".format(mosquito >= beetle))
print("6. Mosquito legs != Beetle legs = {}".format(mosquito != beetle))
print()
